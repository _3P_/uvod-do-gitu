﻿using System;
namespace StringToCamelCase
{
    static class Program
    {
        public static string ToCamelCase(this string text)
        {
            Console.WriteLine("Zadejte oddělovač");
            string oddelovac = Console.ReadLine();
            string[] words = text.Split(oddelovac);
            string result = words[0].ToLower();
            for (int i = 1; i < words.Length; i++)
            {
                result +=
                    words[i].Substring(0, 1).ToUpper() +
                    words[i].Substring(1);
            }
            return result;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Zadejte text");
            string text = Console.ReadLine();
            string camel = text.ToCamelCase();
            Console.WriteLine(camel);
        }
    }
}
